# minetst thats why
This is a fork of the collection of games found in the 'minetest-game-whynot' collection. I found it to be a well balanced minetest game that also has pleanty of features.
The reason for this fork is for easier maintainance for my private server as well as the addition of mods that were not accepted in the past into minetest whynot proper.

Some of the mods I've added are:

- all of mesecons (instead of just part of them)
- pipeworks
- mobs_animal
- mobs_water
- thats_why_tweaks (added recipies and other tweaks for this game)

## How to install

For general mod installation instructions checkout the [minetest wiki on games](https://wiki.minetest.net/Games#Installing_games).

To download this particular game, simply download this repository as a zip (or just [click here](https://gitlab.com/springbov/minetest-thats-why/-/archive/master/minetest-thats-why-master.zip)), unzip it, rename the resulting folder 'minetest_thats_why', and then place it in your 'games' folder.

### License statement
Each mods does have own free license.
