-- Sticky blocks can be used together with pistons or movestones to push / pull

minetest.register_craft({
	output = "mesecons_stickyblocks:sticky_block_all 1",
	recipe = {
		{"mesecons_materials:glue", "mesecons_materials:glue", "mesecons_materials:glue"},
		{"mesecons_materials:glue", "mesecons_materials:glue", "mesecons_materials:glue"},
		{"mesecons_materials:glue", "mesecons_materials:glue", "mesecons_materials:glue"},
	}
})

minetest.register_craft({
	output = "mesecons_materials:glue 9",
	recipe = {
		{"mesecons_stickyblocks:sticky_block_all"},
	}
})
